package com.princess.sms.service;


import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.princess.sms.model.LanguageList;


public class LanguageServiceImplTest extends AbstractServiceIntegrationTestCase {

  @Autowired
  LanguageService languageService;

  @Test
  public void testLanguageService() {
    LanguageList languageList = languageService.findSupportedLanguages();
    Assert.assertEquals(3, languageList.getCount());
    Assert.assertNotNull(languageList);
  
  }
}
