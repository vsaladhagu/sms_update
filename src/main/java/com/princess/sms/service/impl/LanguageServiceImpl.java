package com.princess.sms.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.princess.sms.ExceptionCode;
import com.princess.sms.exception.SmsException;
import com.princess.sms.model.LanguageList;
import com.princess.sms.service.LanguageService;

/**
 * @author <a href="mailto:fvinluan@hagroup.com">Francis Vinluan</a>
 * @version $Revision: 1.0 $
 */
@Service("languageService")
public class LanguageServiceImpl extends AbstractService
    implements LanguageService {

  private static final Logger LOGGER = LoggerFactory.getLogger(LanguageServiceImpl.class);

  @Autowired
  RestTemplate restTemplate;

  @Value("${shipstar.ws.url}")
  private String shipstarWSURL;

  /**
   * Method findSupportedLanguages.
   * 
   * @return LanguageList
   * @see com.princess.sms.service.LanguageService#findSupportedLanguages()
   */
  @Override
  public LanguageList findSupportedLanguages() {
    StringBuilder wsUrl = new StringBuilder(shipstarWSURL).append("languages/supported");

    try {
      ResponseEntity<LanguageList> response = restTemplate.getForEntity(wsUrl.toString(), LanguageList.class);
      return response.getBody();
    } catch (RestClientException e) {
      LOGGER.warn("Error retrieving supported languages : {}", wsUrl.toString(), e);
      throw new SmsException(ExceptionCode.SUPPORTED_LANGUAGES_NOT_FOUND, e);
    }
  }

}
