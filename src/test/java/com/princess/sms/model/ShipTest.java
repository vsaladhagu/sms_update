package com.princess.sms.model;



public class ShipTest extends AbstractJavaBeanTest<Ship> {

  @Override
  protected Ship getBeanInstance() {
    return new Ship();
  }
}
