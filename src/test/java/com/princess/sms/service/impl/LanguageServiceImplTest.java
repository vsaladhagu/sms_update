package com.princess.sms.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.powermock.modules.junit4.PowerMockRunner;
import org.powermock.reflect.Whitebox;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.princess.sms.model.Language;
import com.princess.sms.model.LanguageList;


@RunWith(PowerMockRunner.class)
public class LanguageServiceImplTest {

  @InjectMocks
  LanguageServiceImpl languageServiceImpl = new LanguageServiceImpl();

  @Mock
  RestTemplate restTemplate;

  @Mock
  String shipstarWSURL;


  @Before
  public void setUp() {
    Whitebox.setInternalState(languageServiceImpl, String.class, shipstarWSURL);
  }

  @Test
  public void testFindSupportedLanguages() {
	  StringBuilder wsUrl = new StringBuilder(shipstarWSURL).append("languages/supported");

	    LanguageList expectedLanguageList = new LanguageList();
	    expectedLanguageList.setCount(3);
	    List<Language> languages =new ArrayList<Language>();
	    Language language=new Language();
	    language.setCountryCode("US");
	    language.setLanguageCode("EN");
	    
	    Language language1=new Language();
	    language1.setCountryCode("JP");
	    language1.setLanguageCode("JA");
	    
	    Language language2=new Language();
	    language2.setCountryCode("CN");
	    language2.setLanguageCode("ZH");
	    
	    languages.add(language);
	    languages.add(language1);
	    languages.add(language2);

	    ResponseEntity<LanguageList> response = new ResponseEntity<LanguageList>(expectedLanguageList, HttpStatus.ACCEPTED);
	    Mockito.when(restTemplate.getForEntity(wsUrl.toString(), LanguageList.class)).thenReturn(response);

	    LanguageList actualLanguageList = languageServiceImpl.findSupportedLanguages();

	    Assert.assertEquals(expectedLanguageList, actualLanguageList);

	    Mockito.verify(restTemplate).getForEntity(wsUrl.toString(), LanguageList.class);
    
    
    
    
  }


}
